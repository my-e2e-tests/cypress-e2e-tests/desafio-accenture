/* global Given, Then, When */
import { Given, Then, When } from 'cypress-cucumber-preprocessor/steps'
import UndefinedInsurance from '../pages/UndefinedInsurance/'

var faker = require('faker')

const makeOption = 'Volvo'
const modelOption = 'Moped'
const cylinderCapacity = '7'
const enginerPerformance = '13'
const dateManufactore = '02/05/2019'
const numberSeats = '6'
const rightHandDrive = 'No'
const numberSeatsMotorCycle = '2'
const fuelType = 'Diesel'
const payload = '580'
const totalWeight = '32687'
const listPrice = '89000'
const licensePriceNumber = '2'
const annualMileage = '67549'
const hobbies = ['CliffDiving', 'Speeding']
const dateBirth = '02/19/1979'
const gender = 'Male'
const country = 'Brazil'
const occupation = 'Farmer'
const website = 'www.google.com'
const startDate = '03/26/2023'
const insuranceSum = '30000000'
const meritRating = 'Malus 13'
const damageInsurance = 'Full Coverage'
const optionalProduct = 'LegalDefenseInsurance'
const countesyCar = 'Yes'
const priceOption = 'Platinum'
const zipCode = '6987456'

const firstNameFakerbr = faker.name.firstName()
const lastNameFakerbr = faker.name.lastName()
const streetAddressFakerbr = faker.address.streetAddress()
const cityFaker = faker.address.city()
const emailFaker = faker.internet.email()
const phone = '5588964873254'
const userNameFaker = faker.internet.userName()
const passwordFaker = faker.internet.password(20)
const commentsFaker = faker.lorem.words(20)

Given('Access insurance page', () => {
	UndefinedInsurance.accessUndefinedInsurance()
})

When('Fill in the vehicle data', () => {
	UndefinedInsurance.selectMake(makeOption)
	UndefinedInsurance.selectModel(modelOption)
	UndefinedInsurance.setCylinderCapacity(cylinderCapacity)
	UndefinedInsurance.setEnginePerformance(enginerPerformance)
	UndefinedInsurance.setDateOfManufactore(dateManufactore)
	UndefinedInsurance.selectNumberOfSeats(numberSeats)
	UndefinedInsurance.radioRightHandDrive(rightHandDrive)
	UndefinedInsurance.selectNumberOfSeatsMotorCycle(numberSeatsMotorCycle)
	UndefinedInsurance.selectFuelType(fuelType)
	UndefinedInsurance.setPayload(payload)
	UndefinedInsurance.setTotalWeight(totalWeight)
	UndefinedInsurance.setListPrice(listPrice)
	UndefinedInsurance.setLicensePriceNumber(licensePriceNumber)
	UndefinedInsurance.setAnnualMileage(annualMileage)
	UndefinedInsurance.clickBtnNextEnterInsurantData()
})

When("Fill in the insured's data", () => {
	UndefinedInsurance.setFirstName(firstNameFakerbr)
	UndefinedInsurance.setLastName(lastNameFakerbr)
	UndefinedInsurance.setDateOfBirth(dateBirth)
	UndefinedInsurance.setGender(gender)
	UndefinedInsurance.setStreetAddress(streetAddressFakerbr)
	UndefinedInsurance.chkHobbies(hobbies)
	UndefinedInsurance.selectCountry(country)
	UndefinedInsurance.setZipCode(zipCode)
	UndefinedInsurance.setCity(cityFaker)
	UndefinedInsurance.selectOccupation(occupation)
	UndefinedInsurance.setWebsite(website)
	UndefinedInsurance.clickBtnNextEnterProductData()
})

When('Fill in the product data', () => {
	UndefinedInsurance.setStartDate(startDate)
	UndefinedInsurance.selectInsuranceSum(insuranceSum)
	UndefinedInsurance.selectMeritRating(meritRating)
	UndefinedInsurance.selectDamageInsurance(damageInsurance)
	UndefinedInsurance.chkOptionalProducts(optionalProduct)
	UndefinedInsurance.selectCountesyCar(countesyCar)
	UndefinedInsurance.clickBtnNextSelectPriceOption()
})

When('Select price option', () => {
	UndefinedInsurance.radioPriceOption(priceOption)
	UndefinedInsurance.clickBtnNextSendQuote()
})

When('Fill in the quote form', () => {
	UndefinedInsurance.setEmail(emailFaker)
	UndefinedInsurance.setPhone(phone)
	UndefinedInsurance.setUserName(userNameFaker)
	UndefinedInsurance.setPassword(passwordFaker)
	UndefinedInsurance.setConfirmPassword(passwordFaker)
	UndefinedInsurance.setComments(commentsFaker)
	UndefinedInsurance.clickBtnSendEmail()
})

Then('The message should be {}', message => {
	UndefinedInsurance.validateMsgEmailSent(message)
})
