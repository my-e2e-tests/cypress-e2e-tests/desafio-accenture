export const ELEMENTS = {
	VEHICLEDATA: {
		breadCrumbTitle: 'span#selectedinsurance',
		selectMake: 'select#make',
		selectModel: 'select#model',
		inputCylinderCapacity: 'input#cylindercapacity',
		inputEnginePerformance: 'input#engineperformance',
		inputDateOfManufactore: 'input#dateofmanufacture',
		selectNumberOfSeats: 'select#numberofseats',
		selectNumberOfSeatsMotorCycle: 'select#numberofseatsmotorcycle',
		radioRightHandDriveYes: 'input#righthanddriveyes',
		radioRightHandDriveNo: 'input#righthanddriveno',
		selectFuelType: 'select#fuel',
		inputPayload: 'input#payload',
		inputTotalWeight: 'input#totalweight',
		inputListPrice: 'input#listprice',
		inputLicensePriceNumber: 'input#licenseplatenumber',
		inputAnnualMileage: 'input#annualmileage',
		btnNextEnterInsurantData: 'button#nextenterinsurantdata',
	},

	INSURANTDATA: {
		inputFirstName: 'input#firstname',
		inputLastName: 'input#lastname',
		inputDateOfBirth: 'input#birthdate',
		radioGenders:
			'section[style="display: block;"] .group .ideal-radiocheck-label input[type=radio]',
		radioGenderMale: 'input#gendermale',
		radioGenderFemale: 'input#genderfemale',
		inputStreetAddress: 'input#streetaddress',
		selectCountry: 'select#country',
		inputZipCode: 'input#zipcode',
		inputCity: 'input#city',
		selectOccupation: 'select#occupation',
		chkHobbies:
			'section[style="display: block;"] > .idealforms-field-checkbox > .group input[type=checkbox]',
		inputWebsite: 'input#website',
		inputPicture: 'input#picture',
		btnNextEnterProductData: 'button#nextenterproductdata',
	},

	PRODUCTDATA: {
		inputStartDate: 'input#startdate',
		selectInsuranceSum: 'select#insurancesum',
		selectMeritRating: 'select#meritrating',
		selectDamageInsurance: '#damageinsurance',
		chkOptionalProducts: {
			euroProtection: 'input#EuroProtection',
			legalDefenderInsurance: 'input#LegalDefenseInsurance',
		},
		selectCountesyCar: 'select#courtesycar',
		btnNextSelectPriceOption: 'button#nextselectpriceoption',
	},

	PRICEOPTION: {
		radioPriceOptions: 'tr > .group input[type=radio]',
		btnNextSendQuote: 'button#nextsendquote',
	},

	SENDQUOTE: {
		inputEmail: 'input#email',
		inputPhone: 'input#phone',
		inputUserName: 'input#username',
		inputPassword: 'input#password',
		inputConfirmPassword: 'input#confirmpassword',
		inputComments: 'textarea#Comments',
		btnSendEmail: 'button#sendemail',
		msgEmailSent: 'div.sweet-alert h2',
	},
}
