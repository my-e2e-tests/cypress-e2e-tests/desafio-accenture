/// <reference types="cypress" />

import { should } from 'chai'

const url = Cypress.config('baseUrl')
const elUndefinedInsurance = require('./elements').ELEMENTS

class UndefinedInsurance {
	accessUndefinedInsurance() {
		cy.visit(url)
		cy.get(elUndefinedInsurance.VEHICLEDATA.breadCrumbTitle).should(
			'be.visible'
		)
	}

	selectMake(makeOption) {
		cy.get(elUndefinedInsurance.VEHICLEDATA.selectMake)
			.should('be.visible')
			.select(makeOption)
	}

	selectModel(modelOption) {
		cy.get(elUndefinedInsurance.VEHICLEDATA.selectModel)
			.should('be.visible')
			.select(modelOption)
	}

	setCylinderCapacity(cylinderCapacity) {
		cy.get(elUndefinedInsurance.VEHICLEDATA.inputCylinderCapacity)
			.should('be.visible')
			.type(cylinderCapacity)
	}

	setEnginePerformance(enginePerformance) {
		cy.get(elUndefinedInsurance.VEHICLEDATA.inputEnginePerformance)
			.should('be.visible')
			.type(enginePerformance)
	}

	setDateOfManufactore(dateManufactore) {
		cy.get(elUndefinedInsurance.VEHICLEDATA.inputDateOfManufactore)
			.should('be.visible')
			.type(dateManufactore)
	}

	selectNumberOfSeats(numberSeats) {
		cy.get(elUndefinedInsurance.VEHICLEDATA.selectNumberOfSeats)
			.should('be.visible')
			.select(numberSeats)
	}

	selectNumberOfSeatsMotorCycle(numberSeatsMotorCycle) {
		cy.get(elUndefinedInsurance.VEHICLEDATA.selectNumberOfSeatsMotorCycle)
			.should('be.visible')
			.select(numberSeatsMotorCycle)
	}

	radioRightHandDrive(rightHandDrive) {
		switch (rightHandDrive) {
			case 'Yes':
				cy.get(elUndefinedInsurance.VEHICLEDATA.radioRightHandDriveYes)
					.should('be.visible')
					.check({ force: true })
				break
			case 'No':
				cy.get(elUndefinedInsurance.VEHICLEDATA.radioRightHandDriveNo)
					.should('be.visible')
					.check({ force: true })
				break
		}
	}

	selectNumberOfSeats(numberSeats) {
		cy.get(elUndefinedInsurance.VEHICLEDATA.selectNumberOfSeats)
			.should('be.visible')
			.select(numberSeats)
	}

	selectFuelType(fuelType) {
		cy.get(elUndefinedInsurance.VEHICLEDATA.selectFuelType)
			.should('be.visible')
			.select(fuelType)
	}

	setPayload(payload) {
		cy.get(elUndefinedInsurance.VEHICLEDATA.inputPayload)
			.should('be.visible')
			.type(payload)
	}

	setTotalWeight(totalWeight) {
		cy.get(elUndefinedInsurance.VEHICLEDATA.inputTotalWeight)
			.should('be.visible')
			.type(totalWeight)
	}

	setListPrice(listPrice) {
		cy.get(elUndefinedInsurance.VEHICLEDATA.inputListPrice)
			.should('be.visible')
			.type(listPrice)
	}

	setLicensePriceNumber(licensePriceNumber) {
		cy.get(elUndefinedInsurance.VEHICLEDATA.inputLicensePriceNumber)
			.should('be.visible')
			.type(licensePriceNumber)
	}

	setAnnualMileage(annualMileage) {
		cy.get(elUndefinedInsurance.VEHICLEDATA.inputAnnualMileage)
			.should('be.visible')
			.type(annualMileage)
	}

	clickBtnNextEnterInsurantData() {
		cy.get(elUndefinedInsurance.VEHICLEDATA.btnNextEnterInsurantData)
			.should('be.visible')
			.click()
	}

	setFirstName(firstName) {
		cy.get(elUndefinedInsurance.INSURANTDATA.inputFirstName)
			.should('be.visible')
			.type(firstName)
	}

	setLastName(lastName) {
		cy.get(elUndefinedInsurance.INSURANTDATA.inputLastName)
			.should('be.visible')
			.type(lastName)
	}

	setDateOfBirth(dateBirth) {
		cy.get(elUndefinedInsurance.INSURANTDATA.inputDateOfBirth)
			.should('be.visible')
			.type(dateBirth)
	}

	setGender(gender) {
		cy.get(elUndefinedInsurance.INSURANTDATA.radioGenders)
			.should('be.visible')
			.check(gender, { force: true })
	}

	setStreetAddress(streetAddress) {
		cy.get(elUndefinedInsurance.INSURANTDATA.inputStreetAddress)
			.should('be.visible')
			.type(streetAddress)
	}

	selectCountry(country) {
		cy.get(elUndefinedInsurance.INSURANTDATA.selectCountry)
			.should('be.visible')
			.select(country)
	}

	setZipCode(zipCode) {
		cy.get(elUndefinedInsurance.INSURANTDATA.inputZipCode)
			.should('be.visible')
			.type(zipCode)
	}

	setCity(city) {
		cy.get(elUndefinedInsurance.INSURANTDATA.inputCity)
			.should('be.visible')
			.type(city)
	}

	selectOccupation(occupation) {
		cy.get(elUndefinedInsurance.INSURANTDATA.selectOccupation)
			.should('be.visible')
			.select(occupation)
	}

	chkHobbies(hobbies) {
		cy.get(elUndefinedInsurance.INSURANTDATA.chkHobbies)
			.should('be.visible')
			.check(hobbies, { force: true })
	}

	setWebsite(website) {
		cy.get(elUndefinedInsurance.INSURANTDATA.inputWebsite)
			.should('be.visible')
			.type(website)
	}

	clickBtnNextEnterProductData() {
		cy.get(elUndefinedInsurance.INSURANTDATA.btnNextEnterProductData)
			.should('be.visible')
			.click()
	}

	setStartDate(startDate) {
		cy.get(elUndefinedInsurance.PRODUCTDATA.inputStartDate)
			.should('be.visible')
			.type(startDate)
	}

	selectInsuranceSum(insuranceSum) {
		cy.get(elUndefinedInsurance.PRODUCTDATA.selectInsuranceSum)
			.should('be.visible')
			.select(insuranceSum)
	}

	selectMeritRating(meritRating) {
		cy.get(elUndefinedInsurance.PRODUCTDATA.selectMeritRating)
			.should('be.visible')
			.select(meritRating)
	}

	selectDamageInsurance(damageInsurance) {
		cy.get(elUndefinedInsurance.PRODUCTDATA.selectDamageInsurance)
			.should('be.visible')
			.select(damageInsurance)
	}

	chkOptionalProducts(optionalProduct) {
		switch (optionalProduct) {
			case 'EuroProtection':
				cy.get(
					elUndefinedInsurance.PRODUCTDATA.chkOptionalProducts.euroProtection
				)
					.should('be.visible')
					.check({ force: true })
			case 'LegalDefenseInsurance':
				cy.get(
					elUndefinedInsurance.PRODUCTDATA.chkOptionalProducts
						.legalDefenderInsurance
				)
					.should('be.visible')
					.check({ force: true })
		}
	}

	selectCountesyCar(countesyCar) {
		cy.get(elUndefinedInsurance.PRODUCTDATA.selectCountesyCar)
			.should('be.visible')
			.select(countesyCar)
	}

	clickBtnNextSelectPriceOption() {
		cy.get(elUndefinedInsurance.PRODUCTDATA.btnNextSelectPriceOption)
			.should('be.visible')
			.click()
	}

	radioPriceOption(priceOption) {
		cy.get(elUndefinedInsurance.PRICEOPTION.radioPriceOptions)
			.should('be.visible')
			.check(priceOption, { force: true })
	}

	clickBtnNextSendQuote() {
		cy.get(elUndefinedInsurance.PRICEOPTION.btnNextSendQuote)
			.should('be.visible')
			.click()
	}

	setEmail(email) {
		cy.get(elUndefinedInsurance.SENDQUOTE.inputEmail)
			.should('be.visible')
			.type(email)
	}

	setPhone(phone) {
		cy.get(elUndefinedInsurance.SENDQUOTE.inputPhone)
			.should('be.visible')
			.type(phone)
	}

	setUserName(userName) {
		cy.get(elUndefinedInsurance.SENDQUOTE.inputUserName)
			.should('be.visible')
			.type(userName)
	}

	setPassword(password) {
		cy.get(elUndefinedInsurance.SENDQUOTE.inputPassword)
			.should('be.visible')
			.type(password)
	}

	setConfirmPassword(password) {
		cy.get(elUndefinedInsurance.SENDQUOTE.inputConfirmPassword)
			.should('be.visible')
			.type(password)
	}

	setComments(comments) {
		cy.get(elUndefinedInsurance.SENDQUOTE.inputComments)
			.should('be.visible')
			.type(comments)
	}

	clickBtnSendEmail() {
		cy.get(elUndefinedInsurance.SENDQUOTE.btnSendEmail)
			.should('be.visible')
			.click()
	}

	validateMsgEmailSent(msg) {
		cy.get(elUndefinedInsurance.SENDQUOTE.msgEmailSent, { timeout: 20 * 1000 })
			.should('be.visible')
			.and('have.text', msg)
	}
}
export default new UndefinedInsurance()
