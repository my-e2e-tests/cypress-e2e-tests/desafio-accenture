Feature: Technical challenge

    Scenario: Filled out and submit the insurance form
        Given Access insurance page
        When Fill in the vehicle data
        When Fill in the insured's data
        When Fill in the product data
        When Select price option
        When Fill in the quote form
        Then The message should be Sending e-mail success!